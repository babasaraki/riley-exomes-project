# riley-exomes-project

Code and configuration repository for Riley's Exome Analysis Project

# Exome Processing Script Order
1. run_bwa-mem2.py
2. run_exome_processing.py
3. call_gvcf.py
4. create_genomedb.py
